using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;


public class SceneLoader
{
    private static SceneLoader _instance = null;
    public static SceneLoader Instance() { return _instance; }

    // Start is called before the first frame update
    public void Init()
    {
        _instance = this;
        
    }

    public void RegisterCallback(UnityAction<Scene, LoadSceneMode> finishLoaded)
    {
        SceneManager.sceneLoaded += finishLoaded;
    }

    public void UnRegisterCallback(UnityAction<Scene, LoadSceneMode> finishLoaded)
    {
        SceneManager.sceneLoaded -= finishLoaded;
    }

    public void ChangeScene(string name)
    {
        LoadingProgress.Instance().EableProgress();
        SceneManager.LoadScene(name);
    }

    public IEnumerator ChangeSceneAsync(string name)
    {
        Debug.Log("Change scene async");
        LoadingProgress.Instance().EableProgress();
        AsyncOperation ao = SceneManager.LoadSceneAsync(name);
        if(ao == null)
        {
            yield break;
        }
        Debug.Log("Disable activation");
        ao.allowSceneActivation = false;
        float loadingRatio = 0.5f;
        while(true)
        {
            LoadingProgress.Instance().UpdateProgress(ao.progress* loadingRatio);
            if (ao.progress > 0.8999f)
            { 
               ao.allowSceneActivation = true;
                break; 
            }
            yield return 0;
        }
    }
}
