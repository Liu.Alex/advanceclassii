using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicFunction : MonoBehaviour
{
    public BasicFunction other;
    private int[] aaa = null;
    private void Awake()
    {

        aaa = new int[10];
        aaa[0] = 1;
      //  Debug.Log("Awake " + gameObject.name);
    }

    private void OnEnable()
    {
       // Debug.Log("OnEnable " + gameObject.name);
    }

    private void OnDisable()
    {
       // Debug.Log("OnDisable " + gameObject.name);
    }

    // Start is called before the first frame update
    void Start()
    {
       // Debug.Log("Start " + gameObject.name);

       // Debug.Log(other.getAAA()[0]);
    }

    public int[] getAAA()
    {
        return aaa;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Update : " + gameObject.name + ":" + Time.realtimeSinceStartup + "  :  " + Time.deltaTime + " : " + Time.unscaledDeltaTime);
    }

    private void FixedUpdate()
    {
     //   Debug.Log("FixedUpdate " + gameObject.name + " : " + Time.fixedDeltaTime);
    }

    private void LateUpdate()
    {
        Debug.Log("LateUpdate " + gameObject.name + " : " + Time.deltaTime);
    }
}
