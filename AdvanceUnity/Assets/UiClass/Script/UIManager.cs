using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    public Image playerHpBar;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MinusHpBtnClick()
    {
        PlayerCtrl.instance.Damage(10);
    }

    public void UpdatePlayerUIInfo(CPlayerData data)
    {
        float ratio = data.hp / data.maxHp;
        if(ratio < 0)
        {
            ratio = 0;
        } else if(ratio > 1)
        {
            ratio = 1;
        }
        playerHpBar.fillAmount = ratio;
    }
}
