using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CPlayerData
{
    public float maxHp;
    public float hp;
} 
